function validirajNaziv(ieNaziv){
    var ieNaziv = document.getElementById("naziv").value;
    var regExp = /^[A-Z][A-Z0-9]{1,5}$/;
    if(!regExp.test(ieNaziv)){
        document.forms[0].naziv.style.backgroundColor = "#F08080";
        return true;
    }
    else{
        document.forms[0].naziv.style.backgroundColor="lightgreen";
        return false;
    }
}
function validirajPocetak(iePocetak){
    var iePocetak = document.getElementById("pocetak").value;
    var time = iePocetak.split(":");
    var h = time[0];
    var m = time[1];
    if((h*60+m)>8 || time==""){
        document.forms[0].pocetak.style.backgroundColor="#F08080";
        return false;
    }
    else{
       document.forms[0].pocetak.style.backgroundColor="lightgreen";  
       return true; 
    }
      
    }
 
 function validirajKraj(iePocetak, ieKraj){
     var iePocetak = document.getElementById("pocetak").value;
     var ieKraj = document.getElementById("kraj").value;
     var vrijemeP = iePocetak.split(":");
     var vrijemeK = ieKraj.split(":");
     var hPocetak = vrijemeP[0];
     var mPocetak = vrijemeP[1];
     var hKraj = vrijemeK[0];
     var mKraj = vrijemeK[1];

     if((hPocetak*60+mPocetak)>(hKraj*60+mKraj) || vrijemeK==""){
         document.forms[0].kraj.style.backgroundColor="#F08080";
         return false;
     }
     else{
        document.forms[0].kraj.style.backgroundColor="lightgreen";  
        return true; 
     }

 }
 
 function validirajTip(ieTip, iePocetak, ieKraj){
     var ieTip = document.getElementById("tip").value;
     var iePocetak = document.getElementById("pocetak").value;
     var ieKraj = document.getElementById("kraj").value;
     var vrijemeP = iePocetak.split(":");
     var vrijemeK = ieKraj.split(":");
     var hPocetak = vrijemeP[0];
     var mPocetak = vrijemeP[1];
     var hKraj = vrijemeK[0];
     var mKraj = vrijemeK[1];
     if(ieTip=="vjezba" && ((hKraj*60-hPocetak*60)>180 || (mKraj-mPocetak)<45)){
        document.forms[0].tip.style.backgroundColor="#F08080";
        return false;
     }
     else if(ieTip=="pred" && ((hKraj*60-hPocetak*60)>180 || (hKraj*60-hPocetak*60)<60) && (mKraj-mPocetak)<60){
        document.forms[0].tip.style.backgroundColor="#F08080";
        return false;
     }
     else{
        document.forms[0].tip.style.backgroundColor="lightgreen";  
        return true; 
     }
 }
    