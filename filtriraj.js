let FiltrirajRaspored = (function(){
    let raspored;
    
    function postaviRaspored(postavljenRaspored){
        raspored=postavljenRaspored;
        return raspored;
    }
    
        // metoda filtrirajPredmet, prikazuje red u kojem se nalazi uneseni predmet :(
   
        $(document).ready(function filtrirajPredmet(){
            $("#pretragaPredmeta").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $(".table tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
    
         
          $(document).ready(function filtrirajTip(){
            $("#pretragaTip").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $(".table tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
    

     $(document).ready(function filtrirajProslo(){
            $("#pretragaDan").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $(".table tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
  });
 
    return{
        postaviRaspored: postaviRaspored,
        filtrirajPredmet: filtrirajPredmet,
        filtrirajTip: filtrirajTip,
        filtrirajProslo: filtrirajProslo
        
    }
}());