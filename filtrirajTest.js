let assert = chai.assert;
describe('FiltrirajRaspored', function(){
    describe('filtrirajPredmet()', function(){
        it('Provjera da li su skriveni predmeti koji ne sadrže uneseni string kada se pozove filtrirajPredmet(wt)', function(){
            FiltrirajRaspored.filtrirajPredmet(wt);
            let tabela = document.getElementsByClassName("table");
            assert.equal(tabela.length, 6, "Treba biti prikazana cijela tabela");
        });
        it('Provjera da li su skrivene aktivnosti koji ne sadrže uneseni string kada se pozove filtrirajTip(vježba)', function(){
            FiltrirajRaspored.filtrirajTip(vježba);
            let tabela = document.getElementsByClassName("table");
            assert.equal(tabela.length, 6, "Treba biti prikazana cijela tabela i skrivene aktivnosti predavanja");
        });
        it('Provjera da li su skriveni dani koji dolaze prije unesenog kada se pozove filtrirajPredmet(Srijeda)', function(){
            FiltrirajRaspored.filtrirajProslo(Srijeda);
            let tabela = document.getElementsByClassName("table");
            assert.equal(tabela.length, 3, "Trebaju biti skriveni dani Ponedjeljak i Utorak a sve ostalo normalno prikazano");
        });
    })
})